//
//  URTermsViewController.swift
//  ureport
//
//  Created by Daniel Amaral on 14/03/16.
//  Copyright © 2016 ilhasoft. All rights reserved.
//

import UIKit

public protocol ISTermsViewControllerDelegate {
    func userDidAcceptTerms(accept:Bool)
}

public class ISTermsViewController: ISModalViewController {
    
    @IBOutlet weak var txtTerms: UITextView!
    @IBOutlet weak var btCancel: UIButton!
    @IBOutlet weak var btAccept: UIButton!
    @IBOutlet weak var viewBackground: UIView!
    
    public var delegate:ISTermsViewControllerDelegate?
    
    private var fileName:String?
    private var fileExtension:String?
    private var setupButtonAsRounded:Bool?
    private var btAcceptColor:UIColor?
    private var btCancelColor:UIColor?
    private var btAcceptTitle:String?
    private var btCancelTitle:String?
    private var btAcceptTitleColor:UIColor?
    private var btCancelTitleColor:UIColor?
    private var setupBackgroundViewAsRounded:Bool?
    
    required public init(fileName:String,fileExtension:String,btAcceptColor:UIColor?, btCancelColor:UIColor?, btAcceptTitle:String?, btCancelTitle:String?, btAcceptTitleColor:UIColor?, btCancelTitleColor:UIColor?, setupButtonAsRounded:Bool?, setupBackgroundViewAsRounded:Bool?) {
        super.init(nibName: "ISTermsViewController", bundle: NSBundle(forClass: ISTermsViewController.self))
        self.fileName = fileName
        self.fileExtension = fileExtension
        self.btAcceptColor = btAcceptColor
        self.btCancelColor = btCancelColor
        self.btAcceptTitle = btAcceptTitle
        self.btCancelTitle = btCancelTitle
        self.btAcceptTitleColor = btAcceptTitleColor
        self.btCancelTitleColor = btCancelTitleColor
        self.setupButtonAsRounded = setupButtonAsRounded
        self.setupBackgroundViewAsRounded = setupBackgroundViewAsRounded
    }

    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        setupLayout()
    }

    override public func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.txtTerms.scrollRangeToVisible(NSMakeRange(0, 0))
        
        if let fileName = fileName {
            setTerms(fileName, fileExtension: fileExtension!)
        }
    }
    
    //MARK: Class Methods
    
    func setupLayout() {
        self.btAccept.backgroundColor = btAcceptColor
        self.btAccept.titleLabel?.text = btAcceptTitle
        self.btAccept.setTitleColor(btAcceptTitleColor, forState: UIControlState.Normal)
        
        self.btCancel.backgroundColor = btCancelColor
        self.btCancel.titleLabel?.text = btCancelTitle
        self.btCancel.setTitleColor(btCancelTitleColor, forState: UIControlState.Normal)
        
        if setupButtonAsRounded == true {
            self.btCancel.layer.cornerRadius = 18
            self.btAccept.layer.cornerRadius = 18
        }
        
        if setupBackgroundViewAsRounded == true {
            self.viewBackground.layer.cornerRadius = 20
        }
        
        setTerms(fileName!, fileExtension: fileExtension!)
    }
    
    public func setTerms(fileName:String,fileExtension:String) {
        
        if let rtfPath = NSBundle.mainBundle().URLForResource(fileName, withExtension: fileExtension) {
            do {
                let attributedStringWithRtf = try NSAttributedString(fileURL: rtfPath, options: [NSDocumentTypeDocumentAttribute:NSRTFTextDocumentType], documentAttributes: nil)
                
                self.txtTerms.attributedText = attributedStringWithRtf
                
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        
    }
    
    //MARK: Button Events

    @IBAction func btCancelTapped(sender: AnyObject) {
        if let delegate = self.delegate {
            delegate.userDidAcceptTerms(false)
        }
    }
    
    @IBAction func btAcceptTapped(sender: AnyObject) {
        if let delegate = self.delegate {
            delegate.userDidAcceptTerms(true)
        }
    }
}
