//
//  LCLucyAlertViewController.swift
//  Lucy
//
//  Created by Daniel Amaral on 17/11/15.
//  Copyright © 2015 Lucy. All rights reserved.
//

import UIKit

public enum LCAlertViewType {
    case Cancel
    case YesOrNo
}

public protocol ISImageAlertViewControllerDelegate {
    func yesButtonTapped()
    func noButtonTapped()
    func cancelButtonTapped()
}

public class ISImageAlertViewController: ISModalViewController {

    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var imgAlert: UIImageView!
    @IBOutlet weak var optionSeparator: UIView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbMessage: UILabel!
    @IBOutlet weak var btCancel: UIButton!
    @IBOutlet weak var btNO: UIButton!
    @IBOutlet weak var btYes: UIButton!
    
    var alertViewType:LCAlertViewType!
    var alertTitle:String!
    var message:String!
    var imageName:String!
    var setupBackgroundViewAsRounded:Bool?
    var imgAlertContentMode:UIViewContentMode?
    
    public var delegate:ISImageAlertViewControllerDelegate!
    
    required public init(alertViewType:LCAlertViewType!,alertTitle:String!,message:String!,imageName:String!,setupBackgroundViewAsRounded:Bool!) {
        self.alertTitle = alertTitle
        self.message = message
        self.alertViewType = alertViewType
        self.imageName = imageName
        self.setupBackgroundViewAsRounded = setupBackgroundViewAsRounded
        self.imgAlertContentMode = UIViewContentMode.ScaleAspectFit
        super.init(nibName: "ISImageAlertViewController", bundle: NSBundle(forClass: ISImageAlertViewController.self))
    }

    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()

        setupLayout()
    }
    
    //MARK: Class Methods
    
    public func setupImgAlertContentMode(contentMode:UIViewContentMode) {
        self.imgAlertContentMode = contentMode
    }
    
    public func setupAlertTitle(alertTitle:String) {
        self.alertTitle = alertTitle
        self.lbTitle.text = self.alertTitle
    }
    
    
    public func setupAlertController(alertViewType:LCAlertViewType) {
        self.alertViewType = alertViewType
        setupLayout()
    }
    
    public func setupMessage(message:String) {
        self.message = message
        self.lbMessage.text = self.message
    }    
    
    func setupLayout() {            
        
        self.lbTitle.text = alertTitle
        self.lbMessage.text = message
        self.viewBackground.layer.cornerRadius = 15
        self.btYes.hidden = self.alertViewType == .Cancel
        self.btNO.hidden = self.alertViewType == .Cancel
        self.btCancel.hidden = self.alertViewType == .YesOrNo
        self.optionSeparator.hidden = self.alertViewType == .Cancel
        self.imgAlert.image = UIImage(named: self.imageName)
        
        if setupBackgroundViewAsRounded == true {
            self.viewBackground.layer.cornerRadius = 20
        }
        
        self.imgAlert.contentMode = self.imgAlertContentMode!
        
    }
    
    //MARK: Button Events
    
    @IBAction func btNOTapped(sender: AnyObject) {
        if let delegate = self.delegate {
            delegate.noButtonTapped()
        }
    }
    
    @IBAction func btYESTapped(sender: AnyObject) {
        if let delegate = self.delegate {
            delegate.yesButtonTapped()
        }
    }
    
    @IBAction func btCancelTapped(sender: AnyObject) {
        if let delegate = self.delegate {
            delegate.cancelButtonTapped()
        }
    }
}
