//
//  ISModalViewController.swift
//  IlhasoftCore
//
//  Created by Daniel Amaral on 01/04/16.
//  Copyright © 2016 ilhasoft. All rights reserved.
//

import UIKit

public class ISModalViewController: UIViewController {

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor!.colorWithAlphaComponent(0)
    }
    
    //MARK: Class Methods
    
    public func close(completion:(finish:Bool) -> Void) {
        UIView.animateWithDuration(0.3, animations: { () -> Void in
            self.view.backgroundColor = self.view.backgroundColor?.colorWithAlphaComponent(0)
        }) { (finished) -> Void in
            self.dismissViewControllerAnimated(true, completion: nil)
            completion(finish: true)
        }
    }
    
    public func show(animated:Bool,inViewController:UIViewController){
        if animated == true {
            self.modalPresentationStyle = UIModalPresentationStyle.OverCurrentContext
            inViewController.presentViewController(self, animated: animated) { () -> Void in
                UIView.animateWithDuration(0.3) { () -> Void in
                    self.view.backgroundColor  = UIColor.blackColor().colorWithAlphaComponent(0.5)
                }
            }
        }
    }

    public func show(animated: Bool, inViewController viewController:UIViewController, withCompletion:(Void) -> Void) {
        if animated == true {
            self.modalPresentationStyle = UIModalPresentationStyle.OverCurrentContext
            viewController.presentViewController(self, animated: animated) { () -> Void in
                UIView.animateWithDuration(0.3) { () -> Void in
                    self.view.backgroundColor  = UIColor.blackColor().colorWithAlphaComponent(0.5)
                    withCompletion()
                }
            }
        }
    }
    
}
