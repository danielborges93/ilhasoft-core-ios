//
//  ISViewControllerUtil.swift
//  IlhasoftCore
//
//  Created by Daniel Borges on 30/04/16.
//  Copyright © 2016 ilhasoft. All rights reserved.
//

import UIKit

var screenSize: CGSize {
    return UIScreen.mainScreen().bounds.size
}

func switchRootViewController(rootViewController: UIViewController, animated: Bool,
                              transition: UIViewAnimationOptions = .TransitionFlipFromLeft,
                              completion: (() -> Void)?) {
    let window = UIApplication.sharedApplication().keyWindow
    if animated {
        UIView.transitionWithView(window!, duration: 0.5, options: transition, animations: {
            let oldState: Bool = UIView.areAnimationsEnabled()
            UIView.setAnimationsEnabled(false)
            window!.rootViewController = rootViewController
            UIView.setAnimationsEnabled(oldState)
            }, completion: { finished in
                if completion != nil {
                    completion!()
                }
        })
    } else {
        window!.rootViewController = rootViewController
    }
}
