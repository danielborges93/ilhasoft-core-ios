//
//  ISDateExtension.swift
//  ureport
//
//  Created by Daniel Amaral on 26/08/15.
//  Copyright (c) 2015 ilhasoft. All rights reserved.
//

import UIKit

public extension NSDate {
    public func yearsFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(NSCalendarUnit.Year, fromDate: date, toDate: self, options: []).year
    }
    public func monthsFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(NSCalendarUnit.Month, fromDate: date, toDate: self, options: []).month
    }
    public func weeksFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(NSCalendarUnit.WeekOfYear, fromDate: date, toDate: self, options: []).weekOfYear
    }
    public func daysFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(NSCalendarUnit.Day, fromDate: date, toDate: self, options: []).day
    }
    public func hoursFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(NSCalendarUnit.Hour, fromDate: date, toDate: self, options: []).hour
    }
    public func minutesFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(NSCalendarUnit.Minute, fromDate: date, toDate: self, options: []).minute
    }
    public func secondsFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(NSCalendarUnit.Second, fromDate: date, toDate: self, options: []).second
    }
    public func offsetFrom(date:NSDate) -> (offset:String?,calendarUnit:NSCalendarUnit?) {
        if yearsFrom(date)   > 0 { return ("\(yearsFrom(date))",.Year)}
        if monthsFrom(date)  > 0 { return ("\(monthsFrom(date))",.Month)}
        if weeksFrom(date)   > 0 { return ("\(weeksFrom(date))",.WeekOfYear)}
        if daysFrom(date)    > 0 { return ("\(daysFrom(date))",.Day)}
        if hoursFrom(date)   > 0 { return ("\(hoursFrom(date))",.Hour)}
        if minutesFrom(date) > 0 { return ("\(minutesFrom(date))",.Minute)}
        if secondsFrom(date) > 0 { return ("\(secondsFrom(date))",.Second)}
        return (nil,nil)
    }
}

