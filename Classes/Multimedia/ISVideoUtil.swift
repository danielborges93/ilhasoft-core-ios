//
//  URVideoUtil.swift
//  ureport
//
//  Created by Daniel Amaral on 02/02/16.
//  Copyright © 2016 ilhasoft. All rights reserved.
//

import UIKit
import AVFoundation

public class ISVideoUtil: NSObject {

    public static let outPutURLDirectory = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)[0] as NSString
    public static let outputURLFile = NSURL(fileURLWithPath: outPutURLDirectory.stringByAppendingPathComponent("video.mp4"))
    
    public class func compressVideo(inputURL: NSURL, handler:(session: AVAssetExportSession) -> Void) {
        
        let urlAsset = AVURLAsset(URL: inputURL, options: nil)
        let exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPresetMediumQuality)!
        
        ISFileUtil.removeFile(outputURLFile)
        
        do {
            try NSFileManager.defaultManager().createDirectoryAtPath(outPutURLDirectory as String, withIntermediateDirectories: true, attributes: nil)
            
            exportSession.outputURL = outputURLFile
            exportSession.shouldOptimizeForNetworkUse = true
            exportSession.outputFileType = AVFileTypeMPEG4
            
            exportSession.exportAsynchronouslyWithCompletionHandler { () -> Void in
                handler(session: exportSession)
            }
            
        } catch let error as NSError {
            print("Creating 'upload' directory failed. Error: \(error)")
        }
        
    }
    
    public class func generateThumbnail(url : NSURL) -> UIImage?{
        let asset = AVAsset(URL: url)
        let assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true

        var time = asset.duration
        time.value = min(time.value, 2)
        
        do {
            let image = try assetImgGenerate.copyCGImageAtTime(time, actualTime: nil)
            let frameImg = UIImage(CGImage: image)
            
            return frameImg
        }catch let error as NSError {
            print("error on generate image thumbnail \(error.localizedDescription)")
            return nil
        }
    }
    
}
