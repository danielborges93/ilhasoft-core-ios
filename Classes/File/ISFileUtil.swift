//
//  ISFileUtil.swift
//  IlhasoftCore
//
//  Created by Daniel Amaral on 02/04/16.
//  Copyright © 2016 ilhasoft. All rights reserved.
//

import UIKit

public class ISFileUtil: NSObject {

    public class func removeFile(fileURL: NSURL) {
        let filePath = fileURL.path
        let fileManager = NSFileManager.defaultManager()
        
        if fileManager.fileExistsAtPath(filePath!) {
            do {
                try fileManager.removeItemAtPath(filePath!)
            }catch let error as NSError {
                print("Can't remove file \(error.localizedDescription)")
            }
            
        }else{
            print("file doesn't exist")
        }
    }
    
}
