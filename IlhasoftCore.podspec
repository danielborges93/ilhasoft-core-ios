Pod::Spec.new do |s|

  s.name         = "IlhasoftCore"
  s.version      = "0.0.2"
  s.summary      = "Libs to improve iOS development"

  s.description  = <<-DESC
                   Libs to improve iOS development
                   DESC

  s.homepage     = "https://bitbucket.org/ilhasoft/ilhasoft-core-ios"
  s.license      = { :type => 'MIT', :file => 'LICENSE' }
  s.author       = { "Daniel Amaral" => "daniel@ilhasoft.com.br" }
  s.social_media_url   = "https://twitter.com/danielamarall"

  s.platform     = :ios
  s.ios.deployment_target = '8.0'
  s.source       = { :git => "https://bitbucket.org/ilhasoft/ilhasoft-core-ios.git", :tag => "0.0.1" }
  s.source_files  = "Classes/**/*.{swift,xib,h,m}"
  s.xcconfig = { 'LIBRARY_SEARCH_PATHS' => '"$(PODS_ROOT)/IlhasoftCore"' }
  s.requires_arc = true
  s.framework = 'UIKit'
  s.framework = 'AVFoundation'
end